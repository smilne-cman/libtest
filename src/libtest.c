#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "libtest.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
void apply_arguments(int argc, char *argv[]);
char* color(int col, const char *message);

/* Global Variables ***********************************************************/
static void (*beforeEach)();
static void (*afterEach)();
static const char *testName;
static int failures = 0;
static int disabled = 0;
static int tests = 0;
static int asserts = 0;

static int argVerbose = FALSE;
static int argMono = FALSE;

void test_init(const char *name, int argc, char *argv[]) {
	apply_arguments(argc, argv);
	testName = name;
	printf( color(WHITE, "%s:\n"), name);
}

void apply_arguments(int argc, char *argv[]) {
	if(argc == 0) {
		return;
	}

	// Skip first argument since it's the name of the executable
	for(int i = 1; i < argc; i++) {
		if(!strcmp(argv[i], "--verbose")) {
			argVerbose = TRUE;
		}

		if(!strcmp(argv[i], "--mono")) {
			argMono = TRUE;
		}
	}
}

void xtest(const char *name, void (*test)()) {
	printf( color(BLACK, "\t%s (disabled)\n"), name);
	disabled++;
}

void test(const char *name, void (*test)()) {
	tests++;
	if(beforeEach != NULL) {
		beforeEach();
	}

	printf( color(WHITE, "\t%s\n"), name);
	test();

	if(afterEach != NULL) {
		afterEach();
	}
}

int test_complete() {
	if(failures > 0) {
		printf( color(RED, "%s failed %i assertions out of %i\n\n"), testName, failures, asserts );
	} else if(disabled > 0) {
		printf( color(WHITE, "%s passed %i tests (%i asserts)(%i disabled tests)\n\n"), testName, tests, asserts, disabled );
	} else {
		printf( color(WHITE, "%s passed %i tests (%i asserts)\n\n"), testName, tests, asserts );
	}

	return failures > 0;
}

void test_before_each(void (*fn)()) {
	beforeEach = fn;
}

void test_after_each(void (*fn)()) {
	afterEach = fn;
}

void test_fail(const char *message) {
	printf("\t\t%s\n", color(RED, message));
	failures++;
}

void test_pass(const char *message) {
	if(argVerbose == TRUE) {
		printf("\t\t%s\n", color(GREEN, message));
	}

	asserts++;
}

void test_log(const char *message) {
	printf("\t\t%s\n", message);
}

void test_assert(int value, const char *message) {
	if(value) {
		test_pass(message);
	} else {
		test_fail(message);
	}
}

void test_assert_int(int actual, int expected, const char *message) {
	if(actual == expected) {
		test_pass(message);
	} else {
		char *msg = (char*)malloc(255 * sizeof(char));
		sprintf(msg, "%s [[Expected %i but got %i]]", message, expected, actual);
		test_fail(msg);
	}
}

void test_assert_char(char actual, char expected, const char *message) {
	if(actual == expected) {
		test_pass(message);
	} else {
		char *msg = (char*)malloc(255 * sizeof(char));
		sprintf(msg, "%s [[Expected '%c' but got '%c']]", message, expected, actual);
		test_fail(msg);
	}
}

void test_assert_float(float actual, float expected, const char *message) {
	if(actual == expected) {
		test_pass(message);
	} else {
		char *msg = (char*)malloc(255 * sizeof(char));
		sprintf(msg, "%s [[Expected '%f' but got '%f']]", message, expected, actual);
		test_fail(msg);
	}
}

void test_assert_long(long actual, long expected, const char *message) {
	if(actual == expected) {
		test_pass(message);
	} else {
		char *msg = (char*)malloc(255 * sizeof(char));
		sprintf(msg, "%s [[Expected '%ld' but got '%ld']]", message, expected, actual);
		test_fail(msg);
	}
}

void test_assert_double(double actual, double expected, const char *message) {
	if(actual == expected) {
		test_pass(message);
	} else {
		char *msg = (char*)malloc(255 * sizeof(char));
		sprintf(msg, "%s [[Expected '%f' but got '%f']]", message, expected, actual);
		test_fail(msg);
	}
}

void test_assert_string(const char *actual, const char *expected, const char *message) {
	if( actual == NULL || strcmp(actual, expected) ) {
		char *msg = (char*)malloc(1025 * sizeof(char));
		sprintf(msg, "%s [[Expected '%s' but got '%s']]", message, expected, actual);
		test_fail(msg);
	} else {
		test_pass(message);
	}
}

char* color(int col, const char *message) {
	char *msg = (char*)malloc(1024 * sizeof(char));

	if(argMono == TRUE) {
		sprintf(msg, "%s", message);
	} else {
		sprintf(msg, "\033[1;%im%s\033[0m", col, message);
	}

	return msg;
}
