#ifndef __libtest__
#define __libtest__

/* Types **********************************************************************/

/* Macros *********************************************************************/
#define TRUE 1
#define FALSE 0

#define BLACK 30
#define RED 31
#define GREEN 32
#define YELLOW 33
#define BLUE 34
#define MAGENTA 35
#define CYAN 36
#define WHITE 37

/* Global Function Prototypes *************************************************/
void test_init(const char *name, int argc, char *argv[]);
void xtest(const char *name, void (*test)());
void test(const char *name, void (*test)());
int test_complete();
void test_before_each(void (*fn)());
void test_after_each(void (*fn)());

void test_fail(const char *message);
void test_pass(const char *message);
void test_log(const char *message);

void test_assert(int value, const char *message);
void test_assert_int(int actual, int expected, const char *message);
void test_assert_char(char actual, char expected, const char *message);
void test_assert_float(float actual, float expected, const char *message);
void test_assert_long(long actual, long expected, const char *message);
void test_assert_double(double actual, double expected, const char *message);
void test_assert_string(const char *actual, const char *expected, const char *message);

#endif
